using UnityEngine;

public class HitRay : MonoBehaviour
{
    private void Update()
    {
        int layerMask = (1 << 8) | (1 << 6);
        if (Physics.Raycast(transform.position, transform.forward, out var hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.green);
            Debug.Log("Hit");
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * 1000, Color.red);
            Debug.Log("Miss");
        }
    }
}