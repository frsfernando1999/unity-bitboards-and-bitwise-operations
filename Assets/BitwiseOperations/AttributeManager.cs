﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class AttributeManager : MonoBehaviour
{
    public static int Magic = 1 << 4;
    public static int Intelligence = 1 << 3;
    public static int Charisma = 1 << 2;
    public static int Fly = 1 << 1;
    public static int Invisible = 1;
    
    public Text attributeDisplay;
    public int attributes = 0;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("MAGIC"))
        {
            attributes ^= Magic;
        }
        else if (other.gameObject.CompareTag("INTELLIGENCE"))
        {
            attributes ^= Intelligence;
        }
        else if (other.gameObject.CompareTag("CHARISMA"))
        {
            attributes ^= Charisma;
        }
        else if (other.gameObject.CompareTag("FLY"))
        {
            attributes ^= Fly;
        }
        else if (other.gameObject.CompareTag("INVISIBLE"))
        {
            attributes ^= Invisible;
        }
        
        else if (other.gameObject.CompareTag("REMOVE"))
        {
            attributes &= ~ (Intelligence | Magic);
        }
        else if (other.gameObject.CompareTag("ADD"))
        {
            attributes |= (Intelligence | Magic | Charisma);
        }
        else if (other.gameObject.CompareTag("RESET"))
        {
            attributes = 0;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(this.transform.position);
        attributeDisplay.transform.position = screenPoint + new Vector3(0,-50,0);
        attributeDisplay.text = Convert.ToString(attributes, 2).PadLeft(8, '0');
        }
       
}
