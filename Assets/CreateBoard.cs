using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CreateBoard : MonoBehaviour
{
    public GameObject[] tilePrefabs;
    public GameObject housePrefab;
    public GameObject treePrefab;
    public Text score;
    public int dirtScoreMultiplier = 10;
    public int desertScoreMultiplier = 2;
    private GameObject[] _tiles;
    [Range(1, 8)] public int size = 8;
    [Range(1, 4)] public int plantTreeTime = 1;
    [Range(1, 4)] public int plantTreeRepeatTime = 1;
    private long _dirtBb;
    private long _desertBb;
    private long _grainBb;
    private long _pastureBb;
    private long _rockBb;
    private long _waterBb;
    private long _woodsBb;
    private long _treeBb;
    private long _playerBb;

    private void Start()
    {
        _tiles = new GameObject[64];
        for (int row = 0; row < size; row++)
        {
            for (int column = 0; column < size; column++)
            {
                int randomTile = Random.Range(0, tilePrefabs.Length);
                Vector3 pos = new Vector3(column, 0, row);
                var tile = Instantiate(tilePrefabs[randomTile], pos, Quaternion.identity);
                tile.name = tile.tag + "_" + row + "_" + column;
                _tiles[row * size + column] = tile;
                if (tile.CompareTag("Dirt")) _dirtBb = SetCellState(_dirtBb, row, column);
                if (tile.CompareTag("Desert")) _desertBb = SetCellState(_desertBb, row, column);
                if (tile.CompareTag("Grain")) _grainBb = SetCellState(_grainBb, row, column);
                if (tile.CompareTag("Pasture")) _pastureBb = SetCellState(_pastureBb, row, column);
                if (tile.CompareTag("Rock")) _rockBb = SetCellState(_rockBb, row, column);
                if (tile.CompareTag("Water")) _waterBb = SetCellState(_waterBb, row, column);
                if (tile.CompareTag("Woods")) _woodsBb = SetCellState(_woodsBb, row, column);
            }
        }

        InvokeRepeating(nameof(PlantTree), plantTreeTime, plantTreeRepeatTime);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                int row = (int)hit.collider.gameObject.transform.position.z;
                int col = (int)hit.collider.gameObject.transform.position.x;
                if (GetCellState((_dirtBb | _desertBb) & ~_treeBb, row, col))
                {
                    GameObject house = Instantiate(housePrefab);
                    house.transform.parent = hit.collider.gameObject.transform;
                    house.transform.localPosition = Vector3.zero;
                    _playerBb = SetCellState(_playerBb, row, col);
                    CalculateScore();
                }
            }
        }
    }

    private void PlantTree()
    {
        int randomRow = Random.Range(0, size);
        int randomColumn = Random.Range(0, size);
        if (GetCellState(_dirtBb & ~_playerBb, randomRow, randomColumn))
        {
            GameObject tree = Instantiate(treePrefab);
            tree.transform.parent = _tiles[randomRow * size + randomColumn].transform;
            tree.transform.localPosition = Vector3.zero;
            _treeBb = SetCellState(_treeBb, randomRow, randomColumn);
        }
    }

    private int CellCount(long bitBoard)
    {
        int count = 0;
        while (bitBoard != 0)
        {
            bitBoard &= bitBoard - 1;
            count++;
        }
        return count;
    }

    void CalculateScore()
    {
        int dirtScore = CellCount(_playerBb & _dirtBb) * dirtScoreMultiplier;
        int desertScore = CellCount(_playerBb & _desertBb) * desertScoreMultiplier;
        int totalScore = dirtScore + desertScore;
        score.text = "Score: " + totalScore;
    }

    private long SetCellState(long bitboard, int row, int col)
    {
        long newBit = 1L << (row * size + col);
        return bitboard | newBit;
    }

    private bool GetCellState(long bitboard, int row, int col)
    {
        long mask = 1L << (row * size + col);
        return (mask & bitboard) != 0;
    }
}